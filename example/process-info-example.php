<?php

require_once __DIR__ . '/../vendor/autoload.php';

$count = 1000;

$processInfo = Am\Utils\ProcessInfo::build($count);

/*for ($i =0; $i < $count; $i++) {
    $processInfo->startOperation();

    // some task
    sleep(1);

    $processInfo->finishOperationWithPrint();
}*/

for ($i =0; $i < $count; $i++) {
    $processInfo->doWithStat(
        function () {
            // some task
            sleep(1);
        }
    );
}