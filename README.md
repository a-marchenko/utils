Utils
=====

Process Info
------------

Useful tool for retrieve information (memory, time progress) while executing some long-lasting loop

    how to use:
```
        $count = 1000;
        $processInfo = \Utils\ProcessInfo::build($count);

        for ($i = 0; $i < $count; $i++) {
            $processInfo->startOperation();

            // some task
            sleep(1);

            $processInfo->finishOperationWithPrint();
        }
```
    or:
```
        $count = 1000;
        $processInfo = \Utils\ProcessInfo::build($count);

        for ($i = 0; $i < $count; $i++) {
            $processInfo->doWithStat(
                function () {

                    // some task
                    sleep(1);

                }
            );
        }
```

