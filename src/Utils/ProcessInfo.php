<?php

namespace Am\Utils;

use \Khill\Duration\Duration;
use \Monolog\Formatter;
use \Rych\ByteSize\ByteSize;

class ProcessInfo
{
    private $totalCount;
    private $step;
    private $formatter;
    private $sumProcessTime = 0;
    private $operationStartTime;
    private $operationInfo = [];
    private $index = 1;

    /**
     * ProcessInfo constructor.
     *
     * @param int $totalCount
     * @param int $step
     */
    public function __construct(int $totalCount, int $step = null)
    {
        if ($totalCount < 1) {
            throw new \InvalidArgumentException('Total count cannot be less then 1');
        }
        if (!is_null($step) and $step < 1) {
            throw new \InvalidArgumentException('step cannot be less then 1');
        }
        $this->totalCount = $totalCount;
        $this->step = is_null($step) ? intval(floor($totalCount / 100)) : $step;
    }

    /**
     * @param int      $totalCount
     * @param int|null $step
     * @return static
     */
    static public function build(int $totalCount, int $step = null)
    {
        return new static($totalCount, $step);
    }

    /**
     * @return $this
     */
    public function startOperation()
    {
        $this->operationStartTime = microtime(true);
        return $this;
    }

    public function finishOperationWithPrint()
    {
        $this->finishOperation();

        if ($this->isAfterFirstOperation() or $this->index % $this->step === 0) {
            $this->printInfo();
        }
    }

    /**
     * @return $this
     */
    public function finishOperation()
    {
        $this->operationInfo = [
            'processed' => round($this->index / $this->totalCount * 100, 2) . '%',
            'memory usage' => [
                'common' => ByteSize::formatBinary(memory_get_usage(true)),
                'real' => ByteSize::formatBinary(memory_get_usage()),
            ],
            'remaining' => "doesn't count"
        ];
        if (!is_null($this->operationStartTime)) {
            $this->sumProcessTime += (microtime(true) - $this->operationStartTime);
            $remainingRaw = (($this->totalCount - $this->index) * $this->sumProcessTime) / $this->index;

            $this->operationInfo['remaining'] = (new Duration())->humanize(intval(floor($remainingRaw)));
        }
        $this->index++;
        return $this;
    }

    public function printInfo()
    {
        echo $this->getFormatter()->format($this->getOperationInfo());
    }

    /**
     * @param callable $operation
     */
    public function doWithStat(callable $operation)
    {
        $this->startOperation();
        $operation();
        $this->finishOperationWithPrint();
    }

    /**
     * @return array
     */
    public function getOperationInfo(): array
    {
        return $this->operationInfo;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->operationStartTime = null;
        $this->sumProcessTime = 0;
        $this->operationInfo = [];
        $this->index = 1;

        return $this;
    }

    /**
     * @param Formatter\FormatterInterface $formatter
     * @return $this
     */
    public function setFormatter(Formatter\FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
        return $this;
    }

    /**
     * @return Formatter\FormatterInterface
     */
    private function getFormatter(): Formatter\FormatterInterface
    {
        if (is_null($this->formatter)) {
            $this->formatter = new Formatter\JsonFormatter();
        }
        return $this->formatter;
    }

    /**
     * @return bool
     */
    private function isAfterFirstOperation(): bool
    {
        return $this->index === 2;
    }
}